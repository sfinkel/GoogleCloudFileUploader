const express = require('express');
const router = express.Router();
const multer = require('multer');
const multerGoogleStorage = require('multer-google-storage');
const sendUploadToGCS = require('../middleware/sendUploadToGCS');

const uploadHandler = multer({
  storage: multerGoogleStorage.storageEngine(),
  limits: {
    fileSize: 5 * 1024 * 1024, // no larger than 5mb, you can change as needed.
  },
});

/*
 *
  curl -XPOST \
  -F "image=@/Users/s/Desktop/notatorrent.torrent" \
  localhost:3003/upload/add
 *
 */
router.post(
  '/add',
  uploadHandler.single('image'), // 'image' here is based form-data in curl cmd
  sendUploadToGCS,
  ({ file: { cloudStoragePublicUrl } = {} }, res, next) => {
    // Was an image uploaded? If so, we'll use its public URL
    // in cloud storage.
    res.json({ publicUrl: cloudStoragePublicUrl }); //req.file.cloudStoragePublicUrl

    // Save the data to the database.
    //   getModel().create(data, (err, savedData) => {
    //     if (err) {
    //       next(err);
    //       return;
    //     }
    //     res.redirect(`${req.baseUrl}/${savedData.id}`);
    //   });
  },
);

module.exports = router;
