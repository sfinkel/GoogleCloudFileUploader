const Storage = require('@google-cloud/storage');
const { GCS_BUCKET, GCLOUD_PROJECT } = process.env;

const storage = Storage({
  //projectId: GCLOUD_PROJECT
  keyFilename: 'gcloud_keys.json',
});
const bucket = storage.bucket(GCS_BUCKET);

function getPublicUrl(filename) {
  return `https://storage.googleapis.com/${GCS_BUCKET}/${filename}`;
}

function sendUploadToGCS(req, res, next) {
  if (!req.file) {
    return next();
  }

  const gcsname = Date.now() + req.file.originalname;
  const file = bucket.file(gcsname);

  const stream = file.createWriteStream({
    metadata: {
      contentType: req.file.mimetype,
    },
    resumable: false,
  });

  stream.on('error', (err) => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on('finish', () => {
    // make the file public
    req.file.cloudStorageObject = gcsname;
    file
      .makePublic()
      .then(() => {
        req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
        next();
      })
      .catch((err) => {
        console.log('err :', err);
        next(err);
      });
  });

  stream.end(req.file.buffer);
}

module.exports = sendUploadToGCS;
